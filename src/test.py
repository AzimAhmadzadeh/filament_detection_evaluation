from __future__ import print_function
import os
from pycocotools import mask as mask
import matplotlib.pyplot as plt


def main0():
    from pycocotools.coco import COCO
    from pycocotools.cocoeval import COCOeval

    ann_type = ['segm', 'bbox']
    ann_type = ann_type[1]
    print('Running demo for *%s* results.' % ann_type)

    gt_file = './gt_dt_jsons/2014_BBSO_filaments.json'
    dt_file = './gt_dt_jsons/bbso_2048_detected_bbox.json'

    # Load GT and DT data
    coco_gt = COCO(gt_file)
    coco_dt = coco_gt.loadRes(dt_file)
    img_ids = sorted(coco_gt.getImgIds())

    coco_eval = COCOeval(coco_gt, coco_dt, ann_type)
    coco_eval.params.imgIds = img_ids
    coco_eval.evaluate()
    coco_eval.accumulate()
    coco_eval.summarize()
    # cocoEval.ious[(20140110180133, 1)][0]


def main1():
    segm = {"size": [2048, 2048],
            "counts": "Tef`17fo15K4K3K11O4L7Jc[i^2"}

    segm2 = {"size": [2048, 2048],
             "counts": "k`ji14jo13N2O01003L4M0O10O10100OO0100000O2O000O1000O1010bPNBVo1g0TQNYOZn1=[QNF;OZn18^QNG84Yo12O2O0O2OO10O0100O100O010000000O010000O1O1001O000000001O001O0001O01O001O1O1O00010O01000O00100O1O10O10O1O2O0O02N01O3N02M1O0010O10O1O10O01O010O010O1O0000000000O100O00010O100O1Oo\\on1"}
    res = mask.decode(segm2)
    print(type(res))
    print(res.shape)
    plt.imshow(res, cmap="Greys")
    plt.show()


def main2():
    from skimage.draw import polygon
    import numpy as np
    segm = [423.32800798801793, 662.3839241138294, 426.34348477284084, 661.375436844733, 429.3589615576635,
            661.375436844733, 430.3574638042936, 659.3684473290066, 429.3589615576635, 656.3529705441838,
            427.3419870194708, 653.337493759361, 427.3419870194708, 650.3320019970045, 427.3419870194708,
            647.3165252121817, 430.3574638042936, 645.3095356964554, 429.3589615576635, 642.2940589116326,
            429.3589615576635, 639.2785821268099, 428.35047428856717, 636.263105341987, 428.35047428856717,
            633.2476285571643, 429.3589615576635, 630.2421367948077, 427.3419870194708, 627.2266600099852,
            426.34348477284084, 624.2111832251624, 424.3364952571144, 625.2196704942587, 423.32800798801793,
            628.225162256615, 423.32800798801793, 631.2406390414378, 422.3195207189216, 634.2561158262606,
            419.31402895656515, 636.263105341987, 416.29855217174236, 636.263105341987, 415.290064902646,
            639.2785821268099, 416.29855217174236, 642.2940589116326, 418.3055416874688, 645.3095356964554,
            419.31402895656515, 648.3150274588118, 418.3055416874688, 651.3305042436346, 419.31402895656515,
            654.3459810284573, 420.31253120319525, 657.3614578132801, 421.3210184722916, 660.3769345981029,
            423.32800798801793, 662.3839241138294]

    img = np.zeros((2048, 2048), dtype=np.uint8)
    y = np.array(segm[1::2])
    x = np.array(segm[0::2])
    yy, xx = polygon(y, x)
    img[yy, xx] = 1
    # plt.imshow(img, cmap="Greys")
    # plt.show()

    img = np.asfortranarray(img)
    encoded = mask.encode(img)
    print(encoded)


def main3():
    segm1 = [423.32800798801793, 662.3839241138294, 426.34348477284084, 661.375436844733, 429.3589615576635,
            661.375436844733, 430.3574638042936, 659.3684473290066, 429.3589615576635, 656.3529705441838,
            427.3419870194708, 653.337493759361, 427.3419870194708, 650.3320019970045, 427.3419870194708,
            647.3165252121817, 430.3574638042936, 645.3095356964554, 429.3589615576635, 642.2940589116326,
            429.3589615576635, 639.2785821268099, 428.35047428856717, 636.263105341987, 428.35047428856717,
            633.2476285571643, 429.3589615576635, 630.2421367948077, 427.3419870194708, 627.2266600099852,
            426.34348477284084, 624.2111832251624, 424.3364952571144, 625.2196704942587, 423.32800798801793,
            628.225162256615, 423.32800798801793, 631.2406390414378, 422.3195207189216, 634.2561158262606,
            419.31402895656515, 636.263105341987, 416.29855217174236, 636.263105341987, 415.290064902646,
            639.2785821268099, 416.29855217174236, 642.2940589116326, 418.3055416874688, 645.3095356964554,
            419.31402895656515, 648.3150274588118, 418.3055416874688, 651.3305042436346, 419.31402895656515,
            654.3459810284573, 420.31253120319525, 657.3614578132801, 421.3210184722916, 660.3769345981029,
            423.32800798801793, 662.3839241138294]
    segm2 = [423.32800798801793, 662.3839241138294, 426.34348477284084, 661.375436844733, 429.3589615576635,
            661.375436844733, 430.3574638042936, 659.3684473290066, 429.3589615576635, 656.3529705441838,
            427.3419870194708, 653.337493759361, 427.3419870194708, 650.3320019970045, 427.3419870194708,
            647.3165252121817, 430.3574638042936, 645.3095356964554, 429.3589615576635, 642.2940589116326,
            654.3459810284573, 420.31253120319525, 657.3614578132801, 421.3210184722916, 660.3769345981029,
            423.32800798801793, 662.3839241138294]
    res1 = mask.frPyObjects([segm1], 2048, 2048)
    res2 = mask.frPyObjects([segm2], 2048, 2048)
    iou = mask.iou(res1, res2, [0])
    print(iou)


def main4():
    from src import combine_filaments as cf
    path_to_gt = 'gt_dt_jsons/2014_BBSO_filaments.json'
    res = cf.combine_gt_filaments(path_to_gt)
    print(res[20140110180133][0])
    '''
    area : <class 'numpy.ndarray'> = [324]
    bbox : <class 'numpy.ndarray'> = [405.24732901 613.16205691  36.16135796  59.26450325]
    segm : <class 'list'> = [{'size': [2048, 2048], 'counts': b'ncni02lo15M1O2]PNG[o1b0L4M2M2I7L300O04_OXQNDjn1LSQN75KUo1NnPN0P[UU3'}]
    '''

    path_to_dt = 'gt_dt_jsons/bbso_2048_detected_segm.json'
    res = cf.combine_dt_filaments(path_to_dt)
    print(res[20140110180133][0])
    '''      
    area : <class 'numpy.uint32'> = 117
    bbox : <class 'numpy.ndarray'> = [405.24732901 613.16205691  36.16135796  59.26450325]
    segm : <class 'dict'> = {'size': [2048, 2048], 'counts': 'Tef`17fo15K4K3K11O4L7Jc[i^2'}
    '''


def main5():
    # From: https://github.com/cocodataset/cocoapi/blob/master/PythonAPI/pycocoEvalDemo.ipynb
    import CONSTANTS as CONST
    from pycocotools.coco import COCO
    from pycocotools.cocoeval import COCOeval

    path_to_dt = os.path.join(CONST.ROOT_DIR,
                              'gt_dt_jsons/bbso_2048_2014_detected_segm.json')
    path_to_gt = os.path.join(CONST.ROOT_DIR,
                              'gt_dt_jsons/2014_BBSO_filaments.json')

    coco_gt = COCO(path_to_gt)
    coco_dt = coco_gt.loadRes(path_to_dt)
    cocoEval = COCOeval(coco_gt, coco_dt, 'segm')
    image_ids = sorted(coco_gt.getImgIds())
    cocoEval.params.imgIds = image_ids
    cocoEval.evaluate()
    cocoEval.accumulate()
    cocoEval.summarize()  # This gives you all variations of APs.
    res = cocoEval.evalImgs  # There is a lot of information here about each image. Come back to this later.


if __name__ == "__main__":
    main5()
