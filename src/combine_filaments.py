import json
import numpy as np
from skimage.draw import polygon
from src.annotation import Annotation as Annotation
from pycocotools import mask as mask

'''
There are two main methods in this module using which
one could convert both ground-truth (gt) and detected
(dt) data into a unified format. These methods are:
 - combine_gt_filaments: works on ground-truth segmentations
 - combine_dt_filaments: works on detected segmentations.
 
 The result in either case is a dictionarie, like:
        {
         <image_id> : [a1: Annotation, a2: Annotation, ... ],
         ...
        }
        
Each record contains all filaments, reported by HEK,
that correspond to one image. The entire dictionary
contains all filaments present in the given json file. 
'''


def combine_gt_filaments(path_to_file: str):
    """
    creates a list of dictionaries, each with two keys, 'image_id'
    and 'annos'. The former is the id of an image, and the latter
    is an object of type Annotation.
    :param path_to_file: relative path to where the json file
    of the filament dataset (coco-style) is stored.
    :return: a list of dictionaries.
    """
    with open(path_to_file, "r") as read_file:
        data: dict = json.load(read_file)

    imgs_list = data.get('images')  # a list of dicts: each dict describes one image
    anno_list = data.get('annotations')  # a list of dicts: each dict describes one annotation

    all_image_ids = [d.get('id') for d in imgs_list]

    # ----------------------------------------------
    # Create a dictionary of image ids (as key) and lists of
    # annotations (as value). The id is the string of characters
    # already provided for each image, and the annotations are
    # gathered from the json file, and put into objects of type
    # 'Annotation'.
    # ----------------------------------------------
    total_n_images = len(all_image_ids)
    i = 0
    image_annos_dict = dict()
    for image_id in all_image_ids:
        i = i + 1
        # print('[{}] / [{}]'.format(i, total_n_images))
        annos_in_this_image = []
        for anno_dict in anno_list:
            if anno_dict['image_id'] == image_id:
                segm: list = anno_dict['segmentation'][0]
                # segm: dict = convert_poly_to_rle(segm, (2048, 2048))
                # Replaced the above line with this line below followin
                # the example in line 265 of:
                # filament_detection_evaluation/venv/lib64/python3.7/site-packages/pycocotools/coco.py
                segm: dict = mask.frPyObjects([segm], 2048, 2048)
                area = mask.area(segm)
                anno_obj = Annotation(area, anno_dict['bbox'], segm)
                annos_in_this_image.append(anno_obj)

        image_annos_dict.update({image_id: annos_in_this_image})

    return image_annos_dict


def combine_dt_filaments(path_to_segm_file: str):
    """
    creates a list of dictionaries, each with two keys, 'image_id'
    and 'annos'. The former is the id of an image, and the latter
    is an object of type Annotation.
    :param path_to_segm_file: relative path to where the json file
    of the filament dataset (coco-style) is stored.
    :return: a list of dictionaries.
    """
    with open(path_to_segm_file, "r") as read_file:
        data: list = json.load(read_file)

    # Get all (unique) image ids
    all_image_ids = list(set([d['image_id'] for d in data]))

    # ----------------------------------------------
    # Create a dictionary of image ids (as key) and lists of
    # annotations (as value). The id is the string of characters
    # already provided for each image, and the annotations are
    # gathered from the json file, and put into objects of type
    # 'Annotation'.
    # ----------------------------------------------
    total_n_images = len(all_image_ids)
    i = 0
    image_annos_dict = dict()
    for image_id in all_image_ids:
        i = i + 1
        # print('[{}] / [{}]'.format(i, total_n_images))
        annos_in_this_image = []
        for d in data:
            if d['image_id'] == image_id:
                segm: dict = d['segmentation']
                area = mask.area(segm)
                anno_obj = Annotation(area, np.empty(4, dtype=float), segm)
                annos_in_this_image.append(anno_obj)

        image_annos_dict.update({image_id: annos_in_this_image})

    return image_annos_dict


def convert_poly_to_rle(segm: list, image_shape: tuple):
    """
    converts the given polygon to the RLE format, by generating
    the binary mask first, and then using the module 'mask' in
    cocoapi to encode the mask into the RLE formant.
    :param segm: a list of polygons, as shown below, where the
    coordinates are of the same scale as the image.
        segm = [[x1,y1,...,xN,yN],...,[x1,y1,x2,y2…xN,yN]]
    :param image_shape: shape of the image the mask should be
    created for.
    :return: a dictionary with two keys;
        {'size' : [w,h], 'counts' : str}
    """
    segm_mask = np.zeros(image_shape, dtype=np.uint8)
    y = np.array(segm[1::2])
    x = np.array(segm[0::2])
    yy, xx = polygon(y, x)
    segm_mask[yy, xx] = 1
    segm_mask = np.asfortranarray(segm_mask)
    encoded = mask.encode(segm_mask)
    return encoded
