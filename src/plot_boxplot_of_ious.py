import json
import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
import CONSTANTS as CONST


# Load IoUs
path_to_file = os.path.join(CONST.ROOT_DIR, 'ious/ious_2014_onebyone.json')
with open(path_to_file, "r") as read_file:
    data1: dict = json.load(read_file)

path_to_file = os.path.join(CONST.ROOT_DIR, 'ious/ious_2014_batch.json')
with open(path_to_file, "r") as read_file:
    data2: dict = json.load(read_file)

out_filename = 'boxplots_of_IoUs_2014.svg'
# ---------------------------------------
#               BOXPLOT
# ---------------------------------------
sorted_ids = sorted(data1)  # get keys in an ordered fashion
sorted_vals_1 = [data1[id] for id in sorted_ids]  # get values in the order keys are sorted
sorted_vals_2 = [data2[id] for id in sorted_ids]  # get values in the order keys are sorted

# Only for the first 30 days
sorted_ids = sorted_ids[:30]
sorted_vals_1 = sorted_vals_1[:30]
sorted_vals_2 = sorted_vals_2[:30]
# --------- sorted(data1) -----------#

fig, ax1 = plt.subplots(figsize=(15, 5))
ax1.set_xlabel('H-alpha Image Id')
ax1.set_ylabel('IoU')
fig.canvas.set_window_title('A Boxplot Example')
fig.subplots_adjust(left=0.075, right=0.95, top=0.9, bottom=0.25)

xtickNames = plt.setp(ax1, xticklabels=sorted_ids)
plt.setp(xtickNames, rotation=45, fontsize=8, horizontalalignment='right')

bp = ax1.boxplot(sorted_vals_1, notch=0, sym='+', vert=1, whis=1.5)
plt.setp(bp['boxes'], color='black', linewidth=0.4)
plt.setp(bp['whiskers'], color='black', linewidth=0.4)
plt.setp(bp['fliers'], marker='o', color='blue')
plt.setp(bp['medians'], color='cyan')

box_colors = ['#323232']
num_boxes = len(sorted_vals_1)
medians = np.empty(num_boxes)
for i in range(num_boxes):
    box = bp['boxes'][i]
    boxX = []
    boxY = []
    for j in range(5):
        boxX.append(box.get_xdata()[j])  # x values of bl, br, tr, tl, and bl corners
        boxY.append(box.get_ydata()[j])  # y values of bl, br, tr, tl, and bl corners
    box_coords = np.column_stack([boxX, boxY])
    ax1.add_patch(Polygon(box_coords, facecolor=box_colors[0]))
    med = bp['medians'][i]
    # draw markers at 'mean'
    ax1.plot(np.average(med.get_xdata()), np.average(sorted_vals_1[i]),
             color=box_colors[0], marker='s', markeredgecolor='yellow')
    # Add IoUs from data2
    ax1.plot(i+1, sorted_vals_2[i], color='yellow', markeredgecolor='black', marker='X')

# plt.show()
plt.savefig(out_filename, format="svg")


# ---------------------------------------
# Source: [https://matplotlib.org/3.1.1/gallery/statistics/boxplot_demo.html]
# Markers: [https://matplotlib.org/3.1.1/api/markers_api.html]


# Do the followings experiments:
# 1. Compare combined masks of filaments in gt and dt
# (the results should be identical to the above results)
# 2. Do detection for 2015 and 2016
# 3. Run detection over one month worth of images, and
# create a catalogue of bad detections. Then do the same
# using RCNN model. If you could find out the cases were
# the issue seems resolved, a great case would be made.
