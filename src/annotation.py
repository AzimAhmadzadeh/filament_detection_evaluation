import copy
import numpy as np
import sys


class Annotation:
    """
    A class that keeps the information about the detected area. The class
    fields are as follows:
     - area: (float) the area of the detected object.
     - bbox: (np.ndarray) the bounding box of the detected object.
     - segmentation: (dict) a dictionary of two values, the image size, and the encoded
    segmentation in RLE format. Example of segmentation:
        segmentation = {'size': [2048, 2048], 'counts': 'Tef`17fo15K4K3K11O4L7Jc[i^2'}

    Note: The RLE format is chosen as the preferred format for segmentation
    simply because it consumes much less space, compared to binary masks. The former
    is a string of characters, while the latter is a matrix of the same size as
    the original images are (e.g., 2k-by-2k).

    Note: The segmentation (RLE format) in Annotation objects can be easily converted
    to the binary mask, by using the 'mask' module from cocoapi:
        anno = Annotation(...)
        bimask: np.ndarray = mask.decode(anno.segmentation)
    """
    def __init__(self, a: float, b: np.ndarray, s: dict):
        self.area = a
        self.bbox: np.ndarray = b
        self.segmentation: dict = s

    @property
    def bbox(self):
        return self._bbox

    @property
    def segmentation(self):
        return self._segmentation

    @property
    def area(self):
        return self._area

    @area.setter
    def area(self, a: float):
        self._area = a

    @bbox.setter
    def bbox(self, b: np.ndarray):
        self._bbox = np.empty_like(b, dtype=float)
        self._bbox[:] = b

    @segmentation.setter
    def segmentation(self, s: dict):
        self._segmentation = np.empty_like(s, dtype=float)
        self._segmentation = copy.deepcopy(s)

    def __repr__(self):
        pr = '''area : {} = {}\nbbox : {} = {}\nsegm : {} = {}\n
             '''.format(type(self.area), self.area, type(self.bbox), self.bbox,
                        type(self.segmentation), self.segmentation)
        return pr

    def getsizeof(self):
        """
        computes the size of memory (in bytes) consumed by this instance
        of the class.
        :return: the memory size in bytes.
        """
        return sys.getsizeof(self._bbox) + sys.getsizeof(self._segmentation)


def main():
    a = 12.0
    b = np.array([133.18318771842237, 224.3534323514728, 16.827333999001496, 10.799625561657479])
    s = {'size': [2048, 2048], 'counts': 'Tef`17fo15K4K3K11O4L7Jc[i^2'}
    # s = np.array([[136.69970044932603, 232.64041437843235, 137.45356964553173, 232.39078881677483],
    #      [138.2074388417374, 232.13866699950074, 138.9588117823265, 232.13866699950074]])
    ann = Annotation(a, b, s)
    print(ann)
    b[:] = b * 0
    print(ann)
    # print(ann.area)
    # print(ann.bbox)
    # print(ann.segmentation)


if __name__ == '__main__':
    main()
