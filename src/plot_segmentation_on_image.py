import numpy as np
import matplotlib.pyplot as plt
from PIL import Image, ImageDraw
from pycocotools import mask as Mask
from src import combine_filaments as cf

# ----------------------------------------
# An image id to be analyzed
# ----------------------------------------
image_id = 20140111185917

# ----------------------------------------
# Paths to the detected (dt) and ground-truth (gt) data
# ----------------------------------------
path_to_dt = 'gt_dt_jsons/bbso_2048_2014_detected_bbox.json'
path_to_gt = 'gt_dt_jsons/2014_BBSO_filaments.json'
path_to_image = 'bbso_samples/bbso_halph_fr_20140111_185917.jpg'

# ----------------------------------------
# Load and unify the gt and dt results
# ----------------------------------------
gt_dict = cf.combine_gt_filaments(path_to_gt)
dt_dict = cf.combine_dt_filaments(path_to_dt)

# ----------------------------------------
# Take all gt segmentations (in binary form) present in this image
# ----------------------------------------
all_gt_segms = []
for anno in gt_dict[image_id]:
    all_gt_segms.append(Mask.decode(anno.segmentation))

# ----------------------------------------
# Take all dt segmentations (in binary form) present in this image
# ----------------------------------------
all_dt_segms = []
for anno in dt_dict[image_id]:
    all_dt_segms.append(Mask.decode(anno.segmentation))

# ----------------------------------------
# Plot the raw image
# ----------------------------------------
image = Image.open(path_to_image)
figure = plt.figure()
plt.figure(dpi=150)  # or use figsize = (8,8)
plt.axis('off')
plt.imshow(image, cmap='gray')
plt.show()

# ----------------------------------------
# Plot the dt segmentations
# ----------------------------------------
im1 = image.convert('RGBA')  # because we want to draw masks in colors
im1.putalpha(150)  # adding alpha channel (to have transparency in masks)

overlay_filaments = Image.new('RGBA', im1.size, (255, 255, 255, 0))
img_draw = ImageDraw.Draw(overlay_filaments)

# Draw mask of each filament as a new layer
for i in range(len(all_dt_segms)):
    a_mask = all_dt_segms[i]
    np_mask = np.asarray(a_mask)  # convert tensor to array
    np_mask[np_mask == 1] = 255  # replace 1's with 255's
    layer = Image.fromarray(np_mask, mode='L')  # create a layer from the mask
    img_draw.bitmap((0, 0), layer, fill='blue')  # draw the layer on the image

# for s, p in zip(all_scores, all_text_pos):
#     img_draw.text(p, str(s), fill= 'darkblue')

# Combine layers with image and show
im1 = Image.alpha_composite(im1, overlay_filaments)
plt.figure(dpi=150)
plt.axis('off')
plt.title('DETECTED SEGMENTATION (with scores)')
plt.imshow(im1)
plt.show()
# plt.savefig('_detected_{}.png'.format(image_id), bbox_inches='tight')


# ----------------------------------------
# Plot the dt segmentations
# ----------------------------------------
im2 = image.convert('RGBA')  # because we want to draw masks in colors
im2.putalpha(150)  # adding alpha channel (to have transparency in masks)

overlay_filaments = Image.new('RGBA', im2.size, (255, 255, 255, 0))
img_draw = ImageDraw.Draw(overlay_filaments)

# Draw mask of each filament as a new layer
for i in range(len(all_gt_segms)):
    a_mask = all_gt_segms[i]
    np_mask = np.asarray(a_mask)  # convert tensor to array
    np_mask = np.squeeze(np_mask)
    np_mask[np_mask == 1] = 255  # replace 1's with 255's
    layer = Image.fromarray(np_mask, mode='L')  # create a layer from the mask
    img_draw.bitmap((0, 0), layer, fill='red')  # draw the layer on the image

# for s, p in zip(all_scores, all_text_pos):
#     img_draw.text(p, str(s), fill= 'darkblue')

# Combine layers with image and show
im2 = Image.alpha_composite(im2, overlay_filaments)
plt.figure(dpi=150)
plt.axis('off')
plt.title('DETECTED SEGMENTATION (with scores)')
plt.imshow(im2)
plt.show()
# plt.savefig('groundTruth_{}.png'.format(image_id))
