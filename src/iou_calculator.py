import numpy as np
import json
from pycocotools import mask as Mask

"""
This script provides a method to compute all IoUs computed
based on the Detected and Ground-truth segmentations.
"""


def cross_check_ids(id_set1: set, id_set2: set) -> bool:
    """
    checks if two sets are identical in terms of their content.
    :param id_set1: set of ids
    :param id_set2: set of ids
    :return: True, if the two sets are identical, and False otherwise.
    """
    if len(id_set1 - id_set2) == 0 and len(id_set2 - id_set1) == 0:
        return True
    else:
        return False


def compute_all_individual_ious(gt_dict: dict, dt_dict: dict):
    """
    computes Intersection over Union (IoU) on all detected (dt) and ground truth (gt)
    segmentations, on each image separately. It does this by iterating over all images
    and compute IoUs between all dt and gt filaments that have any intersection. If IoU
    is zero, then it is assumed that the objects correspond to two different instances.

    Notes:
        1. There are cases where for one dt segmentation, there are multiple gt segmentations
        and vice versa. Suppose two detected segmentations, dt_1 and dt_2, both correspond to
        one gt segmentation. In this method, IoU(dt_1, gt) and IoU(st_2, gt) will be computed
        independently. This results in lower IoUs even when both gt and dt segmentations are
        fairly similar in area but not necessarily in the numbers. To avoid this, see another
        method I implemented here, called 'compute_all_batch_ious'.

        2. The number of objects in two dictionaries are not the same because
        some detected objects do not exist in the gt data, and some existing gt filaments
        perhaps were not detected. Therefore, this method gives results ONLY about those
        that are present in both dictionaries.

    :param gt_dict:
    :param dt_dict:
    :return: a dictionary of image ids and an array of IoUs.
    """
    if not cross_check_ids(set(gt_dict.keys()), set(dt_dict.keys())):
        print('The image IDs of the two dictionaries are not identical.')
        raise NameError('Bad Data')

    image_ids: list = list(dt_dict.keys())

    ious_for_all: dict = {}
    n = len(image_ids)
    i = 1
    for image_id in image_ids:
        print('{}/{}\t: processing now ... [{}]'.format(i, n, image_id))
        i = i + 1

        annos_gt = gt_dict[image_id]
        annos_dt = dt_dict[image_id]

        all_gt_segms = [anno.segmentation[0] for anno in annos_gt]
        all_dt_segms = [anno.segmentation for anno in annos_dt]

        # Compute all pair-wise IoUs, get the non-zero indices, and drop the rest.
        # (The assumption is that if IoU = 0, the gt and dt filaments correspond to
        # two different instances.)
        ious: np.ndarray = Mask.iou(all_gt_segms, all_dt_segms, [False])
        gt_indices, dt_indices = np.nonzero(ious)
        indices = [(x, y) for x, y in zip(gt_indices, dt_indices)]
        ious_nonzero = ious[gt_indices, dt_indices]
        ious_for_all.update({image_id: ious_nonzero.tolist()})

    return ious_for_all


def compute_all_batch_ious(gt_dict: dict, dt_dict: dict):
    """
    computes Intersection over Union (IoU) on all detected (dt) and ground truth (gt)
    segmentations, on each image separately. It does this by iterating over all images,
    creating one single mask of all gt segmentations and one for all dt segmentations
    on each image, and finally computing the IoU between the two masks.

    Notes:
        1. There are cases where for one dt segmentation, there are multiple gt segmentations
        and vice versa. Suppose two detected segmentations, dt_1 and dt_2, both correspond to
        one gt segmentation. In this method, IoU(union(dt_1, dt_2), gt) will be computed. This
        compensates for the decrease in overall IoU caused by multiple IoU calculations for one
        filament. This makes this method different (and maybe more robust) than the other method
        called 'compute_all_individual_ious'.

        2. The number of objects in two dictionaries are not the same because
        some detected objects do not exist in the gt data, and some existing gt filaments
        perhaps were not detected. Therefore, this method gives results ONLY about those
        that are present in both dictionaries.

    :param gt_dict:
    :param dt_dict:
    :return: a dictionary of image ids and an array of IoUs.
    """
    if not cross_check_ids(set(gt_dict.keys()), set(dt_dict.keys())):
        print('The image IDs of the two dictionaries are not identical.')
        raise NameError('Bad Data')

    image_ids: list = list(dt_dict.keys())

    ious_for_all: dict = {}
    n = len(image_ids)
    i = 1
    for image_id in image_ids:
        print('{}/{}\t: processing now ... [{}]'.format(i, n, image_id))
        i = i + 1
        annos_gt: list = gt_dict[image_id]
        annos_dt: list = dt_dict[image_id]

        # get all segmentations on this image
        all_gt_segms = [anno.segmentation[0] for anno in annos_gt]
        all_dt_segms = [anno.segmentation for anno in annos_dt]

        # convert all segmentations into (binary) masks
        all_gt_masks = [Mask.decode(segm) for segm in all_gt_segms]
        all_dt_masks = [Mask.decode(segm) for segm in all_dt_segms]

        # collapse all masks into one single mask that contains all
        # filaments. In order to do this we can use OR operation on
        # binary masks, which is similar to UNION of all filaments.
        gt_mask = np.bitwise_or.reduce(all_gt_masks)
        dt_mask = np.bitwise_or.reduce(all_dt_masks)

        # convert binary mask back to REU segmentation
        # (note that you need to convert it to a fortran
        # array to be contiguous.
        gt_segms = Mask.encode(np.asfortranarray(gt_mask))
        dt_segms = Mask.encode(np.asfortranarray(dt_mask))

        # compute IoU (one float number for the two masks)
        ious: np.ndarray = Mask.iou([gt_segms], [dt_segms], [False])
        ious_for_all.update({image_id: ious[0][0]})

    return ious_for_all


def main():
    import os
    from src import combine_filaments as cf
    import CONSTANTS as CONST

    path_to_dt = os.path.join(CONST.ROOT_DIR,
                              'gt_dt_jsons/bbso_2048_2016_detected_segm.json')
    path_to_gt = os.path.join(CONST.ROOT_DIR,
                              'gt_dt_jsons/2016_BBSO_filaments.json')

    year = '2016'
    dt_dict = cf.combine_dt_filaments(path_to_dt)
    gt_dict = cf.combine_gt_filaments(path_to_gt)

    # ious: dict = compute_all_individual_ious(gt_dict, dt_dict)
    ious: dict = compute_all_batch_ious(gt_dict, dt_dict)
    out_filename = 'ious_{}_batch.json'.format(year)
    with open(out_filename, 'w') as json_file:
        json.dump(ious, json_file)


if __name__ == "__main__":
    main()
