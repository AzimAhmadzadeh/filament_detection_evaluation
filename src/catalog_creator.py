import numpy as np
import matplotlib.pyplot as plt
from PIL import Image, ImageDraw
from pycocotools import mask as Mask
from src import combine_filaments as cf
import os


def cross_check_ids(id_set1: set, id_set2: set) -> bool:
    """
    checks if two sets are identical in terms of their content.
    :param id_set1: set of ids
    :param id_set2: set of ids
    :return: True, if the two sets are identical, and False otherwise.
    """
    if len(id_set1 - id_set2) == 0 and len(id_set2 - id_set1) == 0:
        return True
    else:
        return False


def convert_id_to_path(parent_path, image_id):
    """
    converts an id to an abs-path of the corresponding image.
    For example:
        image_id = '20140111185917'
        parent_path = '/data/bbso_coco_2048/images_2014/'
        output = '/data/bbso_coco_2048/images_2014/bbso_halph_fr_20140111_185917.jpg'
    :param parent_path:
    :param image_id:
    :return:
    """
    image_id = str(image_id)
    file_name = 'bbso_halph_fr_?_?.jpg'
    data_str = image_id[:8]
    time_str = image_id[8:]
    file_name = file_name.replace('?', data_str, 1)
    file_name = file_name.replace('?', time_str, 1)
    p = os.path.join(parent_path, file_name)
    return p


def draw_segmentations_on_image(path_to_image, all_segms, plt_title, out_name, segm_col="blue"):
    """
    produces a plot of all filament segmentations overlaid on the corresponding BBSO image.
    :param path_to_image:
    :param all_segms:
    :param plt_title:
    :param out_name:
    :param segm_col:
    :return:
    """
    # ----------------------------------------
    # Plot the dt segmentations
    # ----------------------------------------
    image = Image.open(path_to_image)

    im = image.convert('RGBA')  # to draw masks in colors
    im.putalpha(300)  # to have transparency in masks

    overlay_filaments = Image.new('RGBA', im.size, (255, 255, 255, 0))
    img_draw = ImageDraw.Draw(overlay_filaments)

    # Draw mask of each filament as a new layer
    for i in range(len(all_segms)):
        a_mask = all_segms[i]
        np_mask = np.asarray(a_mask)  # convert tensor to array
        np_mask = np.squeeze(np_mask)
        np_mask[np_mask == 1] = 255  # replace 1's with 255's
        layer = Image.fromarray(np_mask, mode='L')  # create a layer from the mask
        img_draw.bitmap((0, 0), layer, fill=segm_col)  # draw the layer on the image

    # for s, p in zip(all_scores, all_text_pos):
    #     img_draw.text(p, str(s), fill= 'darkblue')
    # Combine layers with image and show
    im = Image.alpha_composite(im, overlay_filaments)
    figure = plt.figure(dpi=300)
    plt.axis('off')
    plt.title(plt_title)
    plt.imshow(im)

    if len(out_name) == 0:
        plt.show()
    else:
        if not os.path.exists(os.path.dirname(out_name)):
            os.makedirs(os.path.dirname(out_name))
    plt.savefig('{}.png'.format(out_name), bbox_inches='tight')
    plt.close(figure)


def main():
    # ----------------------------------------
    # Paths to the detected (dt) and ground-truth (gt) data
    # ----------------------------------------
    path_to_dt = 'gt_dt_jsons/bbso_2048_2014_detected_segm.json'
    path_to_gt = 'gt_dt_jsons/2014_BBSO_filaments.json'
    path_to_images = '/data/bbso_coco_2048/images_2014'
    # ----------------------------------------
    # Load and unify the gt and dt results
    # ----------------------------------------
    print('\n\nLoading Data ...\n')
    gt_dict = cf.combine_gt_filaments(path_to_gt)
    dt_dict = cf.combine_dt_filaments(path_to_dt)

    if not cross_check_ids(set(gt_dict.keys()), set(dt_dict.keys())):
        print('The image IDs of the two dictionaries are not identical.')
        raise NameError('Bad Data')

    image_ids = list(gt_dict.keys())

    # ----------------------------------------
    # Process one image at a time
    # ----------------------------------------
    i = 1
    n = len(image_ids)
    output_path = ''
    for image_id in image_ids:
        print('{}/{} :\tProcessing the image with ID [{}] ...'.format(i, n, image_id))
        i = i + 1
        path_to_image = convert_id_to_path(path_to_images, image_id)

        # ----------------------------------------
        # Take all gt segmentations (in binary form) present in this image
        # ----------------------------------------
        all_gt_segms = []
        for anno in gt_dict[image_id]:
            all_gt_segms.append(Mask.decode(anno.segmentation))

        # ----------------------------------------
        # Draw gt segmentations on BBSO images (and save)
        # ----------------------------------------
        output_path = '/data/BBSO_Catalog/2014/GT'
        output_fname = 'GT_{}'.format(image_id)
        draw_segmentations_on_image(path_to_image, all_gt_segms, 'GROUND TRUTH SEGMENTATION',
                                    os.path.join(output_path, output_fname), 'blue')
        # ----------------------------------------
        # Take all dt segmentations (in binary form) present in this image
        # ----------------------------------------
        all_dt_segms = []
        for anno in dt_dict[image_id]:
            all_dt_segms.append(Mask.decode(anno.segmentation))

        # ----------------------------------------
        # Draw segmentations on BBSO images (and save)
        # ----------------------------------------
        output_path = '/data/BBSO_Catalog/2014/DT'
        output_fname = 'DT_{}'.format(image_id)
        draw_segmentations_on_image(path_to_image, all_dt_segms, 'MASK-RCNN SEGMENTATION',
                                    os.path.join(output_path, output_fname), 'red')

    print('Done.\nThe outputs can be found in: [{}]'.format(output_path))


if __name__ == '__main__':
    main()



